<?php

function check($x, $y, $r) : bool
{
    return check1($x, $y, $r) || check3($x, $y, $r) || check4($x, $y, $r);
}

function check1($x, $y, $r) : bool
{
    return $y >= 0 && ($y <= -2 * $x + $r);
}

function check3($x, $y, $r) : bool
{
    return -$r / 2.0 <= $y && $y <= 0 && -$r / 2.0 <= $x && $x <= 0;
}

function check4($x, $y, $r) : bool
{
    return $y <= 0 && ($y * $y + $x * $x <= $r * $r);
}

$z=$_GET["z"];

echo $z;

return;



$X = $_GET["x"];
$y = $_GET["y"];
$r = $_GET["r"];

$curTime = date("H:i:s", time() - $_GET["curTime"] * 60);


$results = '{"results" : [';

$flag = false;

foreach ($X as $x)
{
    $result_bool = check($x, $y, $r);

    $executionTime = round(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"], 7);
    if($flag)
        $results .= ', ';

    $results .= '{ "x" : "' . $x . '", "y" : "' . $y . '", "r" : "' . $r . '", "curTime" : "' . $curTime . '", "execTime" : "' . $executionTime . '", "result" : "' . ($result_bool ? '+' : '-') . '"}';

    $flag = true;
}

$results .= ']}';

echo $results;
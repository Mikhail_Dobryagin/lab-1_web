$(document).ready(function () {
	
	var y_text_Old = $('#y_text').val();
	
	$('#y_text').on('input', function () {
		var y_text = $('#y_text').val();
		
		if (!checkYChange(y_text))
		{
			$('#y_text').val(y_text_Old);
			return;
		}
		
		y_text_Old = $('#y_text').val();
	});
	
	$('#input_form').on('submit', function (event) {
		
		event.preventDefault();
		
		// if(!checkY($("#y_text").val()))
		// {
			// alert("Неверно введён Y");
			// return false;
		// }
		
		// if($('input[name=r_input]:checked').val() === undefined)
		// {
			// alert("Неверно введён R");
			// return false;
		// }
		
		// if($('input[name=x_input]:checked').val() === undefined)
		// {
			// alert("Неверно введён X");
			// return false;
		// }
		// else
		// {
			// $('input[name=x_input]:checked').each(
				// function () { 
					// alert($(this).val());
				// }
			// );
		// }
		
		let x = '';
		
		$('input[name=x_input]:checked').each( function () {
			if(x != '')
				x += '&';
			
			x += 'x[]=' + $(this).val();
		});
		
		let y = $('input[name=y_input]').val();
		let r = $('input[name=r_input]:checked').val();
		let curTime = new Date().getTimezoneOffset();
		
		$.ajax({
			url: './main.php',
			method: 'GET',
			//data: `x=${x}&y=${y}&r=${r}&time=${curTime}`,
			data: "",
			success: function(response){
				let data = JSON.parse(response);
				for(i in data.results) {
					newRow = '<tr>';
					newRow += '<td class="input_data_column">' + data.results[i].x + '</td>';
					newRow += '<td class="input_data_column">' + data.results[i].y + '</td>';
					newRow += '<td class="input_data_column">' + data.results[i].r + '</td>';
					newRow += '<td class="time_column">' + data.results[i].curTime + '</td>';
					newRow += '<td class="time_column">' + data.results[i].execTime + '</td>';
					newRow += '<td class="result_column"> <img src="img/' + (data.results[i].result === '+' ? 'tick' : 'cross') + '.svg /> </td>';
					newRow += '</tr>';

					('#result_table_body').append(newRow);
				}
			},
			error : function(request, status, error) {
				var statusCode = request.status;
				alert(statusCode);
			}
		});
		
		
	});
	
});

function checkYChange(y_text)
{		
	return y_text != '-' && (Number(y_text) >= 5 || Number(y_text) <=-5 || isNaN(Number(y_text))) ? false : true;
}

function checkY(y_text)
{
	return y_text==='' || Number(y_text) >= 5 || Number(y_text) <=-5 || isNaN(Number(y_text)) ? false : true;
}

function checkX() {
	
}